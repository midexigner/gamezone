import React,{useState } from 'react';
import { Button,Text } from 'react-native'
import Home from './screens/Home';
import ReviewDetails from './screens/ReviewDetails';
import About from './screens/About';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'



const getFonts = () => Font.loadAsync({
  'nunito-regular': require('./assets/fonts/Nunito-Regular.ttf'),
  'nunito-bold': require('./assets/fonts/Nunito-Bold.ttf'),
});

export default function App() {
  const [fontsLoaded, setFontsLoaded] = useState(false);
  const Stack = createStackNavigator();
  if (fontsLoaded) {
    return (
      <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen 
        name="Home" 
        component={Home}
        options={{ 
          title: 'GameZone',
          headerStyle: { backgroundColor: '#eee',height: 60 },
          headerTintColor: '#444',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerTitle: () => <Text>Logo</Text>,
          headerRight: () => (
            <Button
              onPress={() => alert('This is a button!')}
              title="Info"
              color="#000"
            />
          )
         }}
         />
         <Stack.Screen 
         name="ReviewDetails" 
         component={ReviewDetails}
         options={{ 
         title: 'Review Details',
         headerStyle: { backgroundColor: '#eee',height: 60 },
         headerTintColor: '#444',
         }}
          />
         <Stack.Screen 
         name="About" 
         component={About}
         options={{ 
         title: 'About GameZone',
         headerStyle: { backgroundColor: '#eee',height: 60 },
         headerTintColor: '#444',
         }}
          />
      </Stack.Navigator>
    </NavigationContainer>
    );
  } else {
    return (
      <AppLoading 
        startAsync={getFonts} 
        onFinish={() => setFontsLoaded(true)} 
      />
    )
}

}

