import React,{useState } from 'react';
import { Button,Text } from 'react-native'
import Home from './screens/Home';
import ReviewDetails from './screens/ReviewDetails';
import About from './screens/About';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
// drawer
import { createDrawerNavigator} from '@react-navigation/drawer';
import Header from './components/Header';

import { Feather,AntDesign } from '@expo/vector-icons';


const getFonts = () => Font.loadAsync({
  'nunito-regular': require('./assets/fonts/Nunito-Regular.ttf'),
  'nunito-bold': require('./assets/fonts/Nunito-Bold.ttf'),
});

const Stack = createStackNavigator();

const Drawer = createDrawerNavigator();

function HomeStack({navigation}) {
return (
  <Stack.Navigator>
  <Stack.Screen 
  name="Home" 
  component={Home}
  options={{ 
    title: 'GameZone',
    headerStyle: { backgroundColor: '#eee',height: 60 },
    headerTintColor: '#444',
    headerTitleStyle: {  fontWeight: 'bold',},
     headerTitle: () =>  <Header title="GameZone" navigation={navigation} />,
    headerRight: () => (<Button onPress={() => alert('This is a button!')} title="Info" color="#000"/>)
   }}
   />
   <Stack.Screen 
   name="ReviewDetails" 
   component={ReviewDetails}
   options={{ 
   title: 'Review Details',
   headerStyle: { backgroundColor: '#eee',height: 60 },
   headerTintColor: '#444',
   headerRight: () => (<Button onPress={() => alert('This is a button!')} title="Info" color="#000"/>)
   }}
    />
   <Stack.Screen 
   name="About" 
   component={About}
   options={{ 
   title: 'About GameZone',
   headerStyle: { backgroundColor: '#eee',height: 60 },
   headerTitle: () =>  <Header title="GameZone" navigation={navigation} />,
   }}
    />
</Stack.Navigator>
);
}

export default function App() {
  const [fontsLoaded, setFontsLoaded] = useState(false);
  

  if (fontsLoaded) {
    return (
      <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={HomeStack} />
        <Drawer.Screen name="About" component={About} />
      </Drawer.Navigator>
    </NavigationContainer>
    );
  } else {
    return (
      <AppLoading 
        startAsync={getFonts} 
        onFinish={() => setFontsLoaded(true)} 
      />
    )
}

}