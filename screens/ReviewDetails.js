import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import Card from "../components/Card";
import { globalStyles } from "../styles/global";

const ReviewDetails = ({ route, navigation }) => {
  const { title, body, rating } = route.params;
  const pressHandler = () => {
    navigation.goBack();
  };
  return (
    <View style={globalStyles.container}>
      <Card>
      <Text>{title}</Text>
      <Text>{body}</Text>
      <Text>{rating}</Text>
      </Card>
    </View>
  );
};

export default ReviewDetails;

const styles = StyleSheet.create({
  container: {
    padding: 24,
  },
});
